package com.task.service.impl;

import com.task.domain.ItemPrice;
import com.task.exception.InvalidInputException;
import com.task.service.ItemPriceService;
import com.task.util.DateUtils;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service class implementation for 'ItemPrice' entity.
 *
 * Created by Mikhail Kholodkov
 *         on April 6, 2018
 */
public class ItemPriceServiceImpl implements ItemPriceService {

    /**
     * Invalid input exception message.
     * One of the incoming parameters is NULL.
     */
    private static final String INVALID_INPUT_NULL_VALUE_MESSAGE = "One of the incoming fields contain NULL value. New prices cannot be applied!";

    @Override
    public List<ItemPrice> applyNewPrices(List<ItemPrice> existingPrices, List<ItemPrice> newPrices) {
        // Validating incoming values
        validatePricesChangeRequest(existingPrices, newPrices);

        // Incoming values are valid applying new prices
        List<ItemPrice> result = new ArrayList<>();
        result.addAll(existingPrices);
        result.addAll(newPrices);

        // Looking for empty existing prices which have a new price
        Map<ItemPrice, ItemPrice> emptyExistingPrices = new HashMap<>();

        for (ItemPrice newItemPrice : existingPrices) {
            existingPrices
                    .stream()
                    .filter(item -> item.getId().equals(newItemPrice.getId())) // filter by ID
                    .filter(item -> item.getValue() == null || item.getValue() == 0L) // filter by empty price
                    .collect(Collectors.toList())
                    .forEach(item -> emptyExistingPrices.put(item, newItemPrice)); // populating 1:1 map with empty prices
        }

        // Looking for overlapped existing/new price timeline within a single department/item
        Map<ItemPrice, ItemPrice> overlappedItemPrices = new HashMap<>();

        for (ItemPrice newItemPrice : newPrices) {
            existingPrices
                    .stream()
                    .filter(item -> item.getDepartmentNumber().equals(newItemPrice.getDepartmentNumber())) // filter by department
                    .filter(item -> item.getNumber().equals(newItemPrice.getNumber())) // filter by product
                    .filter(item -> DateUtils.isDateTimelineIntersects(
                            newItemPrice.getEffectiveFrom(), newItemPrice.getEffectiveTo(),
                            item.getEffectiveFrom(), item.getEffectiveTo())) // check if dates overlapping
                    .collect(Collectors.toList())
                    .forEach(item -> overlappedItemPrices.put(item, newItemPrice)); // populating 1:1 map with overlapped prices
        }

        // Applying new prices for existing prices without price value
        for (Map.Entry<ItemPrice, ItemPrice> entry : emptyExistingPrices.entrySet()) {
            ItemPrice existingPrice = entry.getKey();
            ItemPrice newPrice = entry.getValue();

            existingPrice.setValue(newPrice.getValue());
        }


        // Adjusting overlapped prices timeline
        for (Map.Entry<ItemPrice, ItemPrice> entry : overlappedItemPrices.entrySet()) {
            ItemPrice existingPrice = entry.getKey();
            ItemPrice newPrice = entry.getValue();

            List<ItemPrice> dividedExistingItemPrices = new ArrayList<>();


            // New price getting inserted before existing price
            if (existingPrice.getEffectiveFrom().after(newPrice.getEffectiveFrom()) &&
                    existingPrice.getEffectiveTo().after(newPrice.getEffectiveTo())) {

                existingPrice.setEffectiveFrom(newPrice.getEffectiveTo());
            }


            // New price getting inserted after existing price
            if (existingPrice.getEffectiveFrom().before(newPrice.getEffectiveFrom()) &&
                    existingPrice.getEffectiveTo().before(newPrice.getEffectiveTo())) {

                existingPrice.setEffectiveTo(newPrice.getEffectiveFrom());
            }


            // Special Case. New price getting inserted in the middle of existing price timeline. Thus this adds one extra price item object
            if (existingPrice.getEffectiveFrom().before(newPrice.getEffectiveFrom()) &&
                    existingPrice.getEffectiveTo().after(newPrice.getEffectiveTo())) {

                // 'Right' price creation
                ItemPrice newItemPrice = new ItemPrice(existingPrice);
                newItemPrice.setEffectiveFrom(newPrice.getEffectiveTo());
                newItemPrice.setEffectiveTo(existingPrice.getEffectiveTo());
                newItemPrice.setId(existingPrice.getId() + 1); // some id change logic

                // 'Left' price adjustment
                existingPrice.setEffectiveTo(newPrice.getEffectiveFrom());

                dividedExistingItemPrices.add(newItemPrice);
            }


            // Adding divided prices to result set, if any
            result.addAll(dividedExistingItemPrices);
        }

        // Searching for existing prices which were completely overridden by new prices
        result.removeAll(
                result.stream()
                .filter(item -> item.getEffectiveFrom().equals(item.getEffectiveTo()) ||
                        item.getEffectiveFrom().after(item.getEffectiveTo()))
                .collect(Collectors.toList()));

        // Sort result set by Price's effective date. Department/item sorting can be added too
        result.sort(Comparator.comparing(ItemPrice::getEffectiveFrom));

        return result;
    }

    /**
     * Validates existing and new prices merge request
     * Validation logic can be extracted as a separate module/class.
     */
    private void validatePricesChangeRequest(List<ItemPrice> existingPrices, List<ItemPrice> newPrices) {
        // Validate non-null values
        if (!Stream.concat(existingPrices.stream(), newPrices.stream()).allMatch(validItemPrice())) {
            throw new InvalidInputException(INVALID_INPUT_NULL_VALUE_MESSAGE);
        }
    }

    /**
     * NULL check predicate.
     * Returns 'false' if any of the 'ItemPrice' fields is NULL. Otherwise returns 'true'.
     */
    private Predicate<ItemPrice> validItemPrice() {
        return item -> item.getId() != null &&
                item.getNumber() != null &&
                item.getDepartmentNumber() != null &&
                item.getProductCode() != null &&
                item.getValue() != null &&
                item.getEffectiveFrom() != null &&
                item.getEffectiveTo() != null; // ID uniqueness and other validation logic can be added
    }
}
