package com.task.service;

import com.task.domain.ItemPrice;

import java.util.List;

/**
 * Service class for 'ItemPrice' entity.
 *
 * Created by Mikhail Kholodkov
 *         on April 6, 2018
 */
public interface ItemPriceService {

    /**
     * Merges new prices with existing prices, considering all required conditions.
     */
    List<ItemPrice> applyNewPrices(List<ItemPrice> existingPrices, List<ItemPrice> newPrices);
}
