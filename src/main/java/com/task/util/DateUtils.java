package com.task.util;

import java.util.Date;

/**
 * Invalid Input Exception.
 * Generic exception which can be thrown upon failed request validation.
 *
 * Created by Mikhail Kholodkov
 *         on April 6, 2018
 */
public final class DateUtils {

    private DateUtils() {
    }

    /**
     * Returns 'true' if one period overlaps or intersects with another period either on the left 'edge', right 'edge' or both.
     * Otherwise returns 'false'
     */
    public static boolean isDateTimelineIntersects(Date periodOneStart, Date periodOneEnd,
                                                   Date periodTwoStart, Date periodTwoEnd) {

        return (periodOneStart.before(periodTwoStart) && periodTwoStart.before(periodOneEnd)) ||
                (periodTwoStart.before(periodOneStart) && periodOneStart.before(periodTwoEnd));
    }
}
