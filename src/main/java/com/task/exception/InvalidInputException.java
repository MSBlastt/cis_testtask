package com.task.exception;

/**
 * Invalid Input Exception.
 * Generic exception which can be thrown upon failed request validation.
 *
 * Created by Mikhail Kholodkov
 *         on April 6, 2018
 */
public final class InvalidInputException extends RuntimeException {

    protected Integer code;

    public InvalidInputException() {
        super();
    }

    public InvalidInputException(String message) {
        super(message);
    }

    public InvalidInputException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public InvalidInputException(String message, Integer code, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}