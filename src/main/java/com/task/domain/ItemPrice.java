package com.task.domain;

import java.util.Date;

/**
 * 'ItemPrice' entity.
 * Represents an item with id, code, department code, price, value and effective timeline dates.
 *
 * Created by Mikhail Kholodkov
 *         on April 6, 2018
 */
public class ItemPrice {

    private Long id;

    private String productCode;

    private Integer number;

    private Integer departmentNumber;

    private Long value;

    private Date effectiveFrom;

    private Date effectiveTo;

    public ItemPrice() {
    }

    // Flat object 'cloning'. Can be done via bundle clone() method or ApacheUtils.BeanUtils.copy(obj, obj)
    public ItemPrice(ItemPrice itemPrice) {
        this.id = itemPrice.id;
        this.productCode = itemPrice.productCode;
        this.number = itemPrice.number;
        this.departmentNumber = itemPrice.departmentNumber;
        this.value = itemPrice.value;
        this.effectiveFrom = itemPrice.effectiveFrom;
        this.effectiveTo = itemPrice.effectiveTo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getDepartmentNumber() {
        return departmentNumber;
    }

    public void setDepartmentNumber(Integer departmentNumber) {
        this.departmentNumber = departmentNumber;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public Date getEffectiveTo() {
        return effectiveTo;
    }

    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }
}
