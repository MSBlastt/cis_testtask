package com.task.test;

import com.task.domain.ItemPrice;
import com.task.service.ItemPriceService;
import com.task.service.impl.ItemPriceServiceImpl;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Unit test cases against service class implementation for 'ItemPrice' entity.
 *
 * Created by Mikhail Kholodkov
 *         on April 6, 2018
 */
public class ItemPriceServiceTest {

    private ItemPriceService itemPriceService = new ItemPriceServiceImpl();

    @Test
    public void testPricesMergeNoDatesOverlap() {
        // Basic scenario. Dates/department/numbers are not overlapping
        List<ItemPrice> existingItemPrices = new ArrayList<>();
        List<ItemPrice> newItemPrices = new ArrayList<>();

        ItemPrice existingPrice = getUniqueItemPrice();
        existingItemPrices.add(existingPrice);

        ItemPrice newItemPrice = getUniqueItemPrice();
        newItemPrice.setEffectiveFrom(new Date(new Date().getTime() + 2_000_000_000));
        newItemPrice.setEffectiveTo(new Date(new Date().getTime() + 3_000_000_000L));

        newItemPrices.add(newItemPrice);


        List<ItemPrice> resultSet = itemPriceService.applyNewPrices(existingItemPrices, newItemPrices);

        // Asserting size
        TestCase.assertEquals(2, resultSet.size());

        // Asserting dates
        for (ItemPrice itemPrice : resultSet) {

            // Existing price coming back unchanged
            if (itemPrice.getId().equals(existingPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), existingPrice.getEffectiveFrom());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), existingPrice.getEffectiveTo());
            }

            // New price coming back unchanged
            if (itemPrice.getId().equals(newItemPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), newItemPrice.getEffectiveFrom());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), newItemPrice.getEffectiveTo());
            }
        }
    }


    @Test
    public void testPricesMergeDatesOverlapBefore() {
        // Dates/department/numbers overlapping. New price getting inserted before old price
        List<ItemPrice> existingItemPrices = new ArrayList<>();
        List<ItemPrice> newItemPrices = new ArrayList<>();

        ItemPrice existingPrice = getUniqueItemPrice();
        existingItemPrices.add(existingPrice);

        // Matching new price with existing price. Changing dates
        ItemPrice newItemPrice = getUniqueItemPrice();
        newItemPrice.setDepartmentNumber(existingPrice.getDepartmentNumber());
        newItemPrice.setNumber(existingPrice.getNumber());
        newItemPrice.setEffectiveFrom(new Date(existingPrice.getEffectiveFrom().getTime() - 1_000_000));
        newItemPrice.setEffectiveTo(new Date(existingPrice.getEffectiveTo().getTime() - 1_000_000));

        newItemPrices.add(newItemPrice);


        List<ItemPrice> resultSet = itemPriceService.applyNewPrices(existingItemPrices, newItemPrices);

        // Asserting size
        TestCase.assertEquals(2, resultSet.size());

        // Asserting dates
        for (ItemPrice itemPrice : resultSet) {

            // Existing price coming back with adjusted timeline
            if (itemPrice.getId().equals(existingPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), newItemPrice.getEffectiveTo());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), existingPrice.getEffectiveTo());
            }

            // New price coming back unchanged
            if (itemPrice.getId().equals(newItemPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), newItemPrice.getEffectiveFrom());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), newItemPrice.getEffectiveTo());
            }
        }
    }

    @Test
    public void testPricesMergeDatesOverlapAfter() {
        // Dates/department/numbers overlapping. New price getting inserted after old price
        List<ItemPrice> existingItemPrices = new ArrayList<>();
        List<ItemPrice> newItemPrices = new ArrayList<>();

        ItemPrice existingPrice = getUniqueItemPrice();
        existingItemPrices.add(existingPrice);

        // Matching new price with existing price. Changing dates
        ItemPrice newItemPrice = getUniqueItemPrice();
        newItemPrice.setDepartmentNumber(existingPrice.getDepartmentNumber());
        newItemPrice.setNumber(existingPrice.getNumber());
        newItemPrice.setEffectiveFrom(new Date(existingPrice.getEffectiveTo().getTime() - 1_000_000));
        newItemPrice.setEffectiveTo(new Date(existingPrice.getEffectiveTo().getTime() + 2_000_000_000));

        newItemPrices.add(newItemPrice);


        List<ItemPrice> resultSet = itemPriceService.applyNewPrices(existingItemPrices, newItemPrices);

        // Asserting size
        TestCase.assertEquals(2, resultSet.size());

        // Asserting dates
        for (ItemPrice itemPrice : resultSet) {

            // Existing price coming back with adjusted timeline
            if (itemPrice.getId().equals(existingPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), existingPrice.getEffectiveFrom());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), newItemPrice.getEffectiveFrom());
            }

            // New price coming back unchanged
            if (itemPrice.getId().equals(newItemPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), newItemPrice.getEffectiveFrom());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), newItemPrice.getEffectiveTo());
            }
        }
    }

    @Test
    public void testPricesMergeDatesOverlapInTheMiddle() {
        // Dates/department/numbers overlapping. New price getting inserted int the middle of old price
        List<ItemPrice> existingItemPrices = new ArrayList<>();
        List<ItemPrice> newItemPrices = new ArrayList<>();

        ItemPrice existingPrice = getUniqueItemPrice();
        existingItemPrices.add(existingPrice);
        Date existingItemPriceEffectiveToDate = existingPrice.getEffectiveTo();

        // Matching new price with existing price. Changing dates
        ItemPrice newItemPrice = getUniqueItemPrice();
        newItemPrice.setDepartmentNumber(existingPrice.getDepartmentNumber());
        newItemPrice.setNumber(existingPrice.getNumber());
        newItemPrice.setEffectiveFrom(new Date(existingPrice.getEffectiveFrom().getTime() + 10_000_000));
        newItemPrice.setEffectiveTo(new Date(existingPrice.getEffectiveTo().getTime() - 10_000_000));

        newItemPrices.add(newItemPrice);


        List<ItemPrice> resultSet = itemPriceService.applyNewPrices(existingItemPrices, newItemPrices);

        // Asserting size
        TestCase.assertEquals(3, resultSet.size());

        // Asserting dates
        for (ItemPrice itemPrice : resultSet) {

            // Existing price coming back with adjusted timeline
            if (itemPrice.getId().equals(existingPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), existingPrice.getEffectiveFrom());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), newItemPrice.getEffectiveFrom());
            }

            // Newly created price (based on existing) coming back with adjusted timeline
            if (itemPrice.getId().equals(existingPrice.getId() + 1)) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), newItemPrice.getEffectiveTo());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), existingItemPriceEffectiveToDate);
            }

            // New price coming back unchanged
            if (itemPrice.getId().equals(newItemPrice.getId())) {
                TestCase.assertEquals(itemPrice.getEffectiveFrom(), newItemPrice.getEffectiveFrom());
                TestCase.assertEquals(itemPrice.getEffectiveTo(), newItemPrice.getEffectiveTo());
            }
        }
    }

    /**
     * Generates and returns pseudo unique Item Price entity
     */
    private ItemPrice getUniqueItemPrice() {
        ItemPrice itemPrice = new ItemPrice();

        itemPrice.setId(new Random().nextLong());
        itemPrice.setNumber(new Random().nextInt());
        itemPrice.setDepartmentNumber(new Random().nextInt());
        itemPrice.setProductCode("some code");
        itemPrice.setValue(new Random().nextLong());
        itemPrice.setEffectiveFrom(new Date(new Date().getTime() - 1_000_000_000));
        itemPrice.setEffectiveTo(new Date(new Date().getTime() + 1_000_000_000));

        return itemPrice;
    }
}
